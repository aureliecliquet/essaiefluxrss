CREATE TABLE `news` (
  `nws_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nws_pseudo` varchar(255) NOT NULL DEFAULT '0',
  `nws_titre` varchar(255) NOT NULL DEFAULT '',
  `nws_contenu` text NOT NULL,
  `nws_date` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nws_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;